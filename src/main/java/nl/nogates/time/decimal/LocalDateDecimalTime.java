package nl.nogates.time.decimal;

import java.time.*;
import java.time.chrono.ChronoLocalDateTime;
import java.time.chrono.ChronoZonedDateTime;
import java.time.temporal.*;
import java.time.zone.ZoneRules;
import java.util.Objects;

import static java.time.temporal.ChronoField.NANO_OF_SECOND;
import static java.time.temporal.ChronoUnit.NANOS;
import static nl.nogates.time.decimal.LocalDecimalTime.*;

/**
 * Decimal time variant of {@link java.time.LocalDateTime}.
 */
public final class LocalDateDecimalTime implements Temporal, ChronoLocalDateTime<LocalDate> {

    /**
     * The minimum supported {@code LocalDateDecimalTime}, '-999999999-01-01T00:00:00'.
     * This is the local date-time of midnight at the start of the minimum date.
     * This combines {@link LocalDate#MIN} and {@link LocalDecimalTime#MIN}.
     * This could be used by an application as a "far past" date-time.
     */
    public static final LocalDateDecimalTime MIN = LocalDateDecimalTime.of(LocalDate.MIN, LocalDecimalTime.MIN);

    public static final LocalDateDecimalTime EPOCH = LocalDateDecimalTime.of(LocalDate.EPOCH, LocalDecimalTime.MIN);

    /**
     * The maximum supported {@code LocalDateDecimalTime}, '+999999999-12-31T23:59:59.999999999'.
     * This is the local date-time just before midnight at the end of the maximum date.
     * This combines {@link LocalDate#MAX} and {@link LocalDecimalTime#MAX}.
     * This could be used by an application as a "far future" date-time.
     */
    public static final LocalDateDecimalTime MAX = LocalDateDecimalTime.of(LocalDate.MAX, LocalDecimalTime.MAX);

    private final LocalDate date;
    private final LocalDecimalTime time;

    private LocalDateDecimalTime(LocalDate date, LocalDecimalTime time) {
        this.date = date;
        this.time = time;
    }

    public static LocalDateDecimalTime of(LocalDate date, LocalDecimalTime time) {
        Objects.requireNonNull(date, "date");
        Objects.requireNonNull(time, "time");
        return new LocalDateDecimalTime(date, time);
    }

    @Override
    public boolean isSupported(TemporalUnit unit) {
        return ChronoLocalDateTime.super.isSupported(unit);
    }

    public static LocalDateDecimalTime ofInstant(Instant instant, ZoneId zone) {
        Objects.requireNonNull(instant, "instant");
        Objects.requireNonNull(zone, "zone");
        LocalDate date = LocalDate.ofInstant(instant, zone);
        LocalDecimalTime decimalTime = LocalDecimalTime.from(LocalTime.ofInstant(instant, zone));
        return new LocalDateDecimalTime(date, decimalTime);
    }

    @Override
    public ChronoLocalDateTime<LocalDate> with(TemporalField field, long newValue) {
        if (field instanceof ChronoField) {
            ChronoField f = (ChronoField) field;
            if (f.isDateBased()) {
                return with(date.with(field, newValue), time);
            }
        }
        if (field instanceof DecimalChronoField) {
            DecimalChronoField f = (DecimalChronoField) field;
            if (f.isTimeBased()) {
                return with(date, time.with(field, newValue));
            }
        }
        return field.adjustInto(this, newValue);
    }

    private LocalDateDecimalTime with(LocalDate newDate, LocalDecimalTime newTime) {
        if (date == newDate && time == newTime) {
            return this;
        }
        return new LocalDateDecimalTime(newDate, newTime);
    }

    @Override
    public LocalDateDecimalTime plus(long amountToAdd, TemporalUnit unit) {
        if (unit instanceof ChronoUnit) {
            ChronoUnit f = (ChronoUnit) unit;
            switch (f) {
                case NANOS:
                    return plusNanos(amountToAdd);
                case MICROS:
                    return plusDays(amountToAdd / MICROS_PER_DAY).plusNanos((amountToAdd % MICROS_PER_DAY) * 1000);
                case MILLIS:
                    return plusDays(amountToAdd / MILLIS_PER_DAY).plusNanos((amountToAdd % MILLIS_PER_DAY) * 1000_000);
                case SECONDS: return plusSeconds(amountToAdd);
                case MINUTES: return plusMinutes(amountToAdd);
                case HOURS: return plusHours(amountToAdd);
                case HALF_DAYS: return plusDays(amountToAdd / 256).plusHours((amountToAdd % 256) * 12);  // no overflow (256 is multiple of 2)
            }
            return with(date.plus(amountToAdd, unit), time);
        }
        if (unit instanceof DecimalChronoUnit) {
            DecimalChronoUnit f = (DecimalChronoUnit) unit;
            switch (f) {
                case DECIMAL_HOURS:
                    return plusDecimalHours(amountToAdd);
                case DECIMAL_MINUTES:
                    return plusDecimalMinutes(amountToAdd);
                case DECIMAL_SECONDS:
                    return plusDecimalSeconds(amountToAdd);
            }
            return with(date.plus(amountToAdd, unit), time);
        }
        return unit.addTo(this, amountToAdd);
    }

    private LocalDateDecimalTime plusHours(long hours) {
        return LocalDateDecimalTime.of(date,
                LocalDecimalTime.from(LocalTime.from(this).plusHours(hours)));
    }

    private LocalDateDecimalTime plusMinutes(long minutes) {
        return LocalDateDecimalTime.of(date,
                LocalDecimalTime.from(LocalTime.from(this).plusMinutes(minutes)));
    }

    private LocalDateDecimalTime plusSeconds(long seconds) {
        return LocalDateDecimalTime.of(date,
                LocalDecimalTime.from(LocalTime.from(this).plusSeconds(seconds)));
    }

    private LocalDateDecimalTime plusNanos(long nanos) {
        return plusWithOverflow(date, 0, 0, 0, nanos, 1);
    }

    private LocalDateDecimalTime plusDecimalSeconds(long seconds) {
        return plusWithOverflow(date, 0, 0, seconds, 0, 1);
    }

    private LocalDateDecimalTime plusDecimalMinutes(long minutes) {
        return plusWithOverflow(date, 0, minutes, 0, 0, 1);
    }

    private LocalDateDecimalTime plusDecimalHours(long hours) {
        return plusWithOverflow(date, hours, 0, 0, 0, 1);
    }

    private LocalDateDecimalTime plusDays(long days) {
        LocalDate newDate = date.plusDays(days);
        return with(newDate, time);
    }

    private LocalDateDecimalTime plusWithOverflow(LocalDate newDate, long hours, long minutes, long seconds, long nanos, int sign) {
        if ((hours | minutes | seconds | nanos) == 0) {
            return with(newDate, time);
        }
        long totDays = nanos / NANOS_PER_DAY +
                seconds / DECIMAL_SECONDS_PER_DAY +
                minutes / DECIMAL_MINUTES_PER_DAY +
                hours / DECIMAL_HOURS_PER_DAY;
        totDays *= sign;
        long totNanos = nanos % NANOS_PER_DAY +
                (seconds % DECIMAL_SECONDS_PER_DAY) * NANOS_PER_DECIMAL_SECOND +
                (minutes % DECIMAL_MINUTES_PER_DAY) * NANOS_PER_DECIMAL_MINUTE +
                (hours % DECIMAL_HOURS_PER_DAY) * NANOS_PER_DECIMAL_HOUR;
        long curNoD = time.toNanoOfDay();
        totNanos = totNanos * sign + curNoD;
        totDays += Math.floorDiv(totNanos, NANOS_PER_DAY);
        long newNoD = Math.floorMod(totNanos, NANOS_PER_DAY);
        LocalDecimalTime newTime = (newNoD == curNoD ? time : LocalDecimalTime.ofNanoOfDay(newNoD));
        return with(newDate.plusDays(totDays), newTime);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <R> R query(TemporalQuery<R> query) {
        if (query == TemporalQueries.chronology() || query == TemporalQueries.zoneId() ||
                query == TemporalQueries.zone() || query == TemporalQueries.offset()) {
            return null;
        } else if (query == TemporalDecimalQueries.localDecimalTime()) {
            return (R) time;
        } else if (query == TemporalQueries.localTime()) {
            return (R) LocalTime.ofNanoOfDay(time.toNanoOfDay());
        } else if (query == TemporalQueries.precision()) {
            return (R) NANOS;
        }
        // inline TemporalAccessor.super.query(query) as an optimization
        // non-JDK classes are not permitted to make this optimization
        return query.queryFrom(this);
    }

    @Override
    public ChronoZonedDateTime<LocalDate> atZone(ZoneId zone) {
        return null;
    }

    @Override
    public long until(Temporal endExclusive, TemporalUnit unit) {
        return 0;
    }

    @Override
    public LocalDate toLocalDate() {
        return null;
    }

    @Override
    public LocalTime toLocalTime() {
        return null;
    }

    @Override
    public String toString() {
        return date.toString() + "T" + time.toString();
    }

    @Override
    public boolean isSupported(TemporalField field) {
        if (field instanceof DecimalChronoField) {
            DecimalChronoField f = (DecimalChronoField) field;
            return f.isTimeBased();
        }
        if (field instanceof ChronoField) {
            ChronoField f = (ChronoField) field;
            return f.isDateBased();
        }
        return field != null && field.isSupportedBy(this);
    }

    @Override
    public long getLong(TemporalField field) {
        return 0;
    }
}
