package nl.nogates.time.decimal;

import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;

public class TemporalDecimalQueries {
    public static TemporalQuery<LocalDecimalTime> localDecimalTime() {
        return TemporalDecimalQueries.LOCAL_DECIMAL_TIME;
    }

    static final TemporalQuery<LocalDecimalTime> LOCAL_DECIMAL_TIME = new TemporalQuery<>() {
        @Override
        public LocalDecimalTime queryFrom(TemporalAccessor temporal) {
            if (temporal.isSupported(ChronoField.NANO_OF_DAY)) {
                return LocalDecimalTime.ofNanoOfDay(temporal.getLong(ChronoField.NANO_OF_DAY));
            }
            return null;
        }

        @Override
        public String toString() {
            return "LocalDecimalTime";
        }
    };
}
