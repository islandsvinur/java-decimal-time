package nl.nogates.time.decimal;

import java.time.temporal.*;

enum DecimalChronoField implements TemporalField {
    NANO_OF_DECIMAL_SECOND("NanoOfDecimalSecond", ChronoUnit.NANOS, DecimalChronoUnit.DECIMAL_SECONDS, ValueRange.of(0, 864_000_000L)),
    MICRO_OF_DECIMAL_SECOND("MicroOfDecimalSecond", ChronoUnit.MICROS, DecimalChronoUnit.DECIMAL_SECONDS, ValueRange.of(0, 999_999)),
    DECIMAL_HOUR_OF_DAY("DecimalHourOfDay", DecimalChronoUnit.DECIMAL_HOURS, ChronoUnit.DAYS, ValueRange.of(0, 9)),
    DECIMAL_MINUTE_OF_DAY("DecimalMinuteOfDay", DecimalChronoUnit.DECIMAL_MINUTES, ChronoUnit.DAYS, ValueRange.of(0, 999L)),
    DECIMAL_SECOND_OF_DAY("DecimalSecondOfDay", DecimalChronoUnit.DECIMAL_SECONDS, ChronoUnit.DAYS, ValueRange.of(0, 99999L)),
    DECIMAL_MINUTE_OF_DECIMAL_HOUR("DecimalMinuteOfDecimalHour", DecimalChronoUnit.DECIMAL_MINUTES, DecimalChronoUnit.DECIMAL_HOURS, ValueRange.of(0, 99)),
    DECIMAL_SECOND_OF_DECIMAL_MINUTE("DecimalSecondOfDecimalMinute", DecimalChronoUnit.DECIMAL_SECONDS, DecimalChronoUnit.DECIMAL_MINUTES, ValueRange.of(0, 99));

    private final String name;
    private final TemporalUnit baseUnit;
    private final TemporalUnit rangeUnit;
    private final ValueRange range;
    private final String displayNameKey;

    DecimalChronoField(String name, TemporalUnit baseUnit, TemporalUnit rangeUnit, ValueRange range) {
        this.name = name;
        this.baseUnit = baseUnit;
        this.rangeUnit = rangeUnit;
        this.range = range;
        this.displayNameKey = null;
    }

    @Override
    public TemporalUnit getBaseUnit() {
        return baseUnit;
    }

    @Override
    public TemporalUnit getRangeUnit() {
        return rangeUnit;
    }

    @Override
    public ValueRange range() {
        return range;
    }

    @Override
    public boolean isDateBased() {
        return false;
    }

    @Override
    public boolean isTimeBased() {
        return true;
    }

    @Override
    public boolean isSupportedBy(TemporalAccessor temporal) {
        return temporal.isSupported(this);
    }

    @Override
    public ValueRange rangeRefinedBy(TemporalAccessor temporal) {
        return temporal.range(this);
    }

    @Override
    public long getFrom(TemporalAccessor temporal) {
        return temporal.getLong(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <R extends Temporal> R adjustInto(R temporal, long newValue) {
        return (R) temporal.with(this, newValue);
    }

    public long checkValidValue(long value) {
        return range().checkValidValue(value, this);
    }

    @Override
    public String toString() {
        return name;
    }
}
