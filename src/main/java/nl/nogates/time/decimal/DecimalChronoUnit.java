package nl.nogates.time.decimal;

import java.time.Duration;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;

public enum DecimalChronoUnit implements TemporalUnit {
    DECIMAL_SECONDS("DecimalSeconds", Duration.ofNanos(1_000_000 / 86_400)),
    DECIMAL_MINUTES("DecimalMinutes", Duration.ofNanos(100_000_000 / 86_400)),
    DECIMAL_HOURS("DecimalHours", Duration.ofNanos(1_000_000_000 / 86_400));

    private final String name;
    private final Duration duration;

    DecimalChronoUnit(String name, Duration estimatedDuration) {
        this.name = name;
        this.duration = estimatedDuration;
    }

    @Override
    public Duration getDuration() {
        return duration;
    }

    @Override
    public boolean isDurationEstimated() {
        return false;
    }

    @Override
    public boolean isDateBased() {
        return false;
    }

    @Override
    public boolean isTimeBased() {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <R extends Temporal> R addTo(R temporal, long amount) {
        return (R) temporal.plus(amount, this);
    }

    @Override
    public long between(Temporal temporal1Inclusive, Temporal temporal2Exclusive) {
        return temporal1Inclusive.until(temporal2Exclusive, this);
    }
}
