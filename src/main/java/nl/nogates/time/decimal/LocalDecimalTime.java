package nl.nogates.time.decimal;

import java.time.DateTimeException;
import java.time.temporal.*;
import java.util.Objects;

import static java.time.temporal.ChronoField.NANO_OF_DAY;
import static nl.nogates.time.decimal.DecimalChronoField.*;

public class LocalDecimalTime implements Temporal, TemporalAdjuster, Comparable<LocalDecimalTime> {
    public static final LocalDecimalTime MIN = of(0);
    public static final LocalDecimalTime MIDNIGHT = MIN;
    public static final LocalDecimalTime NOON = of(5);
    public static final LocalDecimalTime MAX = of(9, 99, 99, 863999999);

    static final int DECIMAL_SECONDS_PER_DECIMAL_MINUTE = 100;
    static final int DECIMAL_MINUTES_PER_DECIMAL_HOUR = 100;
    static final int DECIMAL_SECONDS_PER_DECIMAL_HOUR = DECIMAL_SECONDS_PER_DECIMAL_MINUTE * DECIMAL_MINUTES_PER_DECIMAL_HOUR;

    static final int DECIMAL_HOURS_PER_DAY = 10;
    static final int DECIMAL_MINUTES_PER_DAY = DECIMAL_HOURS_PER_DAY * DECIMAL_MINUTES_PER_DECIMAL_HOUR;
    static final int DECIMAL_SECONDS_PER_DAY = DECIMAL_MINUTES_PER_DAY * DECIMAL_SECONDS_PER_DECIMAL_MINUTE;

    static final long NANOS_PER_DECIMAL_SECOND = 864_000_000L;
    static final long NANOS_PER_DECIMAL_MINUTE = NANOS_PER_DECIMAL_SECOND * DECIMAL_SECONDS_PER_DECIMAL_MINUTE;
    static final long NANOS_PER_DECIMAL_HOUR = NANOS_PER_DECIMAL_MINUTE * DECIMAL_MINUTES_PER_DECIMAL_HOUR;

    static final long NANOS_PER_DAY = NANOS_PER_DECIMAL_HOUR * DECIMAL_HOURS_PER_DAY;
    static final long MICROS_PER_DAY = DECIMAL_SECONDS_PER_DAY * 1000L;
    static final long MILLIS_PER_DAY = DECIMAL_SECONDS_PER_DAY * 1000_000L;

    /**
     * The hour.
     */
    private final int hour;
    /**
     * The minute.
     */
    private final int minute;
    /**
     * The second.
     */
    private final int second;
    /**
     * The nanosecond.
     */
    private final int nanoOfSecond;

    public LocalDecimalTime(int hour, int minute, int second, int nanoOfSecond) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.nanoOfSecond = nanoOfSecond;
    }

    public static LocalDecimalTime from(TemporalAccessor temporal) {
        Objects.requireNonNull(temporal, "temporal");
        LocalDecimalTime time = temporal.query(TemporalDecimalQueries.localDecimalTime());
        if (time == null) {
            throw new DateTimeException("Unable to obtain LocalDecimalTime from TemporalAccessor: " +
                    temporal + " of type " + temporal.getClass()
                                                     .getName());
        }
        return time;
    }

    public static LocalDecimalTime of(int hour) {
        DECIMAL_HOUR_OF_DAY.checkValidValue(hour);
        return new LocalDecimalTime(hour, 0, 0, 0);
    }

    public static LocalDecimalTime of(int hour, int minute) {
        DECIMAL_HOUR_OF_DAY.checkValidValue(hour);
        DECIMAL_MINUTE_OF_DECIMAL_HOUR.checkValidValue(minute);
        return new LocalDecimalTime(hour, minute, 0, 0);
    }

    public static LocalDecimalTime of(int hour, int minute, int second) {
        DECIMAL_HOUR_OF_DAY.checkValidValue(hour);
        DECIMAL_MINUTE_OF_DECIMAL_HOUR.checkValidValue(minute);
        DECIMAL_SECOND_OF_DECIMAL_MINUTE.checkValidValue(second);
        return new LocalDecimalTime(hour, minute, second, 0);
    }

    public static LocalDecimalTime ofNanoOfDay(long nanoOfDay) {
        NANO_OF_DAY.checkValidValue(nanoOfDay);
        int hours = (int) (nanoOfDay / NANOS_PER_DECIMAL_HOUR);
        nanoOfDay -= hours * NANOS_PER_DECIMAL_HOUR;
        int minutes = (int) (nanoOfDay / NANOS_PER_DECIMAL_MINUTE);
        nanoOfDay -= minutes * NANOS_PER_DECIMAL_MINUTE;
        int seconds = (int) (nanoOfDay / NANOS_PER_DECIMAL_SECOND);
        nanoOfDay -= seconds * NANOS_PER_DECIMAL_SECOND;
        return new LocalDecimalTime(hours, minutes, seconds, (int) nanoOfDay);
    }

    public static LocalDecimalTime of(int hour, int minute, int second, int nanoOfSecond) {
        DECIMAL_HOUR_OF_DAY.checkValidValue(hour);
        DECIMAL_MINUTE_OF_DECIMAL_HOUR.checkValidValue(minute);
        DECIMAL_SECOND_OF_DECIMAL_MINUTE.checkValidValue(second);
        NANO_OF_DECIMAL_SECOND.checkValidValue(nanoOfSecond);
        return new LocalDecimalTime(hour, minute, second, nanoOfSecond);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalDecimalTime that = (LocalDecimalTime) o;
        return hour == that.hour && minute == that.minute && second == that.second && nanoOfSecond == that.nanoOfSecond;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hour, minute, second, nanoOfSecond);
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(18);
        int hourValue = hour;
        int minuteValue = minute;
        int secondValue = second;
        int nanoValue = nanoOfSecond;
        buf.append("d")
           .append(hourValue)
           .append(minuteValue < 10 ? ":0" : ":")
           .append(minuteValue);
        if (secondValue > 0 || nanoValue > 0) {
            buf.append(secondValue < 10 ? ":0" : ":")
               .append(secondValue);
            if (nanoValue > 0) {
                buf.append('.');
                if (nanoValue % 1000_000 == 0) {
                    buf.append(Integer.toString((nanoValue / 1000_000) + 1000)
                                      .substring(1));
                } else if (nanoValue % 1000 == 0) {
                    buf.append(Integer.toString((nanoValue / 1000) + 1000_000)
                                      .substring(1));
                } else {
                    buf.append(Integer.toString((nanoValue) + 1000_000_000)
                                      .substring(1));
                }
            }
        }
        return buf.toString();
    }

    @Override
    public boolean isSupported(TemporalField field) {
        if (field instanceof ChronoField) {
            return field == ChronoField.NANO_OF_DAY ||
                    field == ChronoField.MICRO_OF_DAY ||
                    field == ChronoField.MILLI_OF_DAY;
        }
        if (field instanceof DecimalChronoField) {
            return true;
        }
        return field != null && field.isSupportedBy(this);
    }

    @Override
    public long getLong(TemporalField field) {
        if (field instanceof ChronoField) {
            var nanoOfDay = hour * NANOS_PER_DECIMAL_HOUR + minute * NANOS_PER_DECIMAL_MINUTE + second * NANOS_PER_DECIMAL_SECOND + nanoOfSecond;
            switch ((ChronoField) field) {
                case NANO_OF_DAY:
                    return nanoOfDay;
                case MICRO_OF_DAY:
                    return nanoOfDay / 1000;
                case MILLI_OF_DAY:
                    return nanoOfDay / 1000_000;
            }
            throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
        }
        return field.getFrom(this);
    }

    @Override
    public int compareTo(LocalDecimalTime other) {
        int cmp = Integer.compare(hour, other.hour);
        if (cmp == 0) {
            cmp = Integer.compare(minute, other.minute);
            if (cmp == 0) {
                cmp = Integer.compare(second, other.second);
                if (cmp == 0) {
                    cmp = Integer.compare(nanoOfSecond, other.nanoOfSecond);
                }
            }
        }
        return cmp;
    }

    @Override
    public boolean isSupported(TemporalUnit unit) {
        if (unit instanceof DecimalChronoUnit) {
            return unit.isTimeBased();
        }
        if (unit instanceof ChronoUnit) {
            return unit.isTimeBased();
        }
        return unit != null && unit.isSupportedBy(this);
    }

    @Override
    public LocalDecimalTime with(TemporalField field, long newValue) {
        if (field instanceof DecimalChronoField) {
            DecimalChronoField f = (DecimalChronoField) field;
            f.checkValidValue(newValue);
            switch (f) {
                case NANO_OF_DECIMAL_SECOND:
                    return withNano((int) newValue);
                case DECIMAL_SECOND_OF_DECIMAL_MINUTE:
                    return withDecimalSecond((int) newValue);
                case DECIMAL_MINUTE_OF_DECIMAL_HOUR:
                    return withDecimalMinute((int) newValue);
                case DECIMAL_HOUR_OF_DAY:
                    return withDecimalHour((int) newValue);
                case MICRO_OF_DECIMAL_SECOND:
                    return withNano((int) (newValue * 1000));
                case DECIMAL_MINUTE_OF_DAY:
                    return plusDecimalMinutes(newValue - ((long) hour * DECIMAL_MINUTES_PER_DECIMAL_HOUR + minute));
                case DECIMAL_SECOND_OF_DAY:
                    return plusDecimalSeconds(newValue - toDecimalSecondOfDay());
            }
            throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
        }
        if (field instanceof ChronoField) {
            ChronoField f = (ChronoField) field;
            f.checkValidValue(newValue);
            switch (f) {
                case NANO_OF_DAY:
                    return LocalDecimalTime.ofNanoOfDay(newValue);
            }
            throw new UnsupportedTemporalTypeException("Unsupported field: " + field);
        }
        return null;
    }

    private int toDecimalSecondOfDay() {
        int total = hour * DECIMAL_SECONDS_PER_DECIMAL_HOUR;
        total += minute * DECIMAL_SECONDS_PER_DECIMAL_MINUTE;
        total += second;
        return total;
    }

    long toNanoOfDay() {
        return toDecimalSecondOfDay() * NANOS_PER_DECIMAL_SECOND + nanoOfSecond;
    }

    private LocalDecimalTime plusDecimalSeconds(long secondstoAdd) {
        if (secondstoAdd == 0) {
            return this;
        }
        int sofd = hour * DECIMAL_SECONDS_PER_DECIMAL_HOUR +
                minute * DECIMAL_SECONDS_PER_DECIMAL_MINUTE + second;
        int newSofd = ((int) (secondstoAdd % DECIMAL_SECONDS_PER_DAY) + sofd + DECIMAL_SECONDS_PER_DAY) % DECIMAL_SECONDS_PER_DAY;
        if (sofd == newSofd) {
            return this;
        }
        int newHour = newSofd / DECIMAL_SECONDS_PER_DECIMAL_HOUR;
        int newMinute = (newSofd / DECIMAL_SECONDS_PER_DECIMAL_MINUTE) % DECIMAL_MINUTES_PER_DECIMAL_HOUR;
        int newSecond = newSofd % DECIMAL_SECONDS_PER_DECIMAL_MINUTE;
        return of(newHour, newMinute, newSecond, nanoOfSecond);
    }

    private LocalDecimalTime plusDecimalMinutes(long minutesToAdd) {
        if (minutesToAdd == 0) {
            return this;
        }
        int mofd = hour * DECIMAL_MINUTES_PER_DECIMAL_HOUR + minute;
        int newMofd = ((int) (minutesToAdd % DECIMAL_MINUTES_PER_DAY) + mofd + DECIMAL_MINUTES_PER_DAY) % DECIMAL_MINUTES_PER_DAY;
        if (mofd == newMofd) {
            return this;
        }
        int newHour = newMofd / DECIMAL_MINUTES_PER_DECIMAL_HOUR;
        int newMinute = newMofd % DECIMAL_MINUTES_PER_DECIMAL_HOUR;
        return of(newHour, newMinute, second, nanoOfSecond);
    }

    private LocalDecimalTime withDecimalHour(int hour) {
        if (this.hour == hour) {
            return this;
        }
        DECIMAL_HOUR_OF_DAY.checkValidValue(hour);
        return of(hour, minute, second, nanoOfSecond);
    }

    private LocalDecimalTime withDecimalSecond(int second) {
        if (this.second == second) {
            return this;
        }
        DECIMAL_SECOND_OF_DECIMAL_MINUTE.checkValidValue(second);
        return of(hour, minute, second, nanoOfSecond);
    }

    private LocalDecimalTime withDecimalMinute(int minute) {
        if (this.minute == minute) {
            return this;
        }
        DECIMAL_MINUTE_OF_DECIMAL_HOUR.checkValidValue(minute);
        return of(hour, minute, second, nanoOfSecond);
    }

    public LocalDecimalTime withNano(int nanoOfSecond) {
        if (this.nanoOfSecond == nanoOfSecond) {
            return this;
        }
        NANO_OF_DECIMAL_SECOND.checkValidValue(nanoOfSecond);
        return of(hour, minute, second, nanoOfSecond);
    }

    @Override
    public LocalDecimalTime plus(long amountToAdd, TemporalUnit unit) {
        if (unit instanceof DecimalChronoUnit) {
            switch ((DecimalChronoUnit) unit) {
                case DECIMAL_HOURS:
                    return plusDecimalHours(amountToAdd);
                case DECIMAL_MINUTES:
                    return plusDecimalMinutes(amountToAdd);
                case DECIMAL_SECONDS:
                    return plusDecimalSeconds(amountToAdd);
            }
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + unit);
        }
        if (unit instanceof ChronoUnit) {
            switch ((ChronoUnit) unit) {
                case NANOS:
                    return plusNanos(amountToAdd);
                case MICROS:
                    return plusNanos(amountToAdd * 1000);
                case MILLIS:
                    return plusNanos(amountToAdd * 1000_000);
            }
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + unit);
        }
        return unit.addTo(this, amountToAdd);
    }

    private LocalDecimalTime plusNanos(long amountToAdd) {
        return of(hour, minute, second, (int) (nanoOfSecond + amountToAdd));
    }

    private LocalDecimalTime plusDecimalHours(long hoursToAdd) {
        if (hoursToAdd == 0) {
            return this;
        }
        int newHour = ((int) (hoursToAdd % DECIMAL_HOURS_PER_DAY) + hour + DECIMAL_HOURS_PER_DAY) % DECIMAL_HOURS_PER_DAY;
        return of(newHour, minute, second, nanoOfSecond);
    }

    @Override
    public long until(Temporal endExclusive, TemporalUnit unit) {
        LocalDecimalTime end = LocalDecimalTime.from(endExclusive);
        if (unit instanceof DecimalChronoUnit) {
            long decimalSecondsUntil = end.toDecimalSecondOfDay() - toDecimalSecondOfDay();
            switch ((DecimalChronoUnit) unit) {
                case DECIMAL_SECONDS:
                    return decimalSecondsUntil;
                case DECIMAL_MINUTES:
                    return decimalSecondsUntil / DECIMAL_SECONDS_PER_DECIMAL_MINUTE;
                case DECIMAL_HOURS:
                    return decimalSecondsUntil / DECIMAL_SECONDS_PER_DECIMAL_HOUR;
            }
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + unit);
        }
        if (unit instanceof ChronoUnit) {
            long nanosUntil = end.toNanoOfDay() - toNanoOfDay();  // no overflow
            switch ((ChronoUnit) unit) {
                case NANOS:
                    return nanosUntil;
                case MICROS:
                    return nanosUntil / 1000;
                case MILLIS:
                    return nanosUntil / 1000_000;
            }
            throw new UnsupportedTemporalTypeException("Unsupported unit: " + unit);
        }
        return unit.between(this, end);
    }

    @Override
    public Temporal adjustInto(Temporal temporal) {
        return temporal.with(NANO_OF_DAY, toNanoOfDay());
    }
}
