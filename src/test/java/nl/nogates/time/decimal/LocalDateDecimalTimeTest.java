package nl.nogates.time.decimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.time.*;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LocalDateDecimalTimeTest {
    @Test
    void with() {
        var epoch = LocalDateDecimalTime.EPOCH;
        assertEquals("1970-01-01Td0:00",
                epoch.toString());
        assertEquals("1970-01-01Td0:00:43",
                epoch.with(DecimalChronoField.DECIMAL_SECOND_OF_DECIMAL_MINUTE, 43)
                     .toString());
        assertEquals("1970-01-01Td3:43",
                epoch.with(DecimalChronoField.DECIMAL_MINUTE_OF_DAY, 343)
                     .toString());
        assertEquals("2012-05-24Td3:43:32",
                epoch.with(DecimalChronoField.DECIMAL_SECOND_OF_DAY, 34332)
                     .with(ChronoField.DAY_OF_MONTH, 24)
                     .with(ChronoField.MONTH_OF_YEAR, 5)
                     .with(ChronoField.YEAR, 2012)
                     .toString());
    }

    @Test
    void plus() {
        var epoch = LocalDateDecimalTime.EPOCH;

        assertEquals("1970-01-01Td3:00",
                epoch.plus(3, DecimalChronoUnit.DECIMAL_HOURS)
                     .toString());
        assertEquals("1970-01-01Td0:03",
                epoch.plus(3, DecimalChronoUnit.DECIMAL_MINUTES)
                     .toString());
        assertEquals("1970-01-01Td0:00:03",
                epoch.plus(3, DecimalChronoUnit.DECIMAL_SECONDS)
                     .toString());

        assertEquals("1970-01-01Td1:25",
                epoch.plus(3, ChronoUnit.HOURS)
                     .toString());
        assertEquals("1970-01-01Td0:02:08.288",
                epoch.plus(3, ChronoUnit.MINUTES)
                     .toString());
        assertEquals("1970-01-01Td0:00:03.408",
                epoch.plus(3, ChronoUnit.SECONDS)
                     .toString());
    }

    @ParameterizedTest(name = "DecimalChronoField " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    @EnumSource
    void isSupportedDecimalChronoField(DecimalChronoField field) {
        var now = LocalDateDecimalTime.of(
                LocalDate.now(),
                LocalDecimalTime.ofNanoOfDay(
                        LocalTime.now()
                                 .toNanoOfDay()));
        assertTrue(now.isSupported(field));
    }

    @ParameterizedTest(name = "ChronoField " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    @EnumSource
    void isNotSupportedChronoField(ChronoField field) {
        var now = LocalDateDecimalTime.of(
                LocalDate.now(),
                LocalDecimalTime.ofNanoOfDay(
                        LocalTime.now()
                                 .toNanoOfDay()));

        assertEquals(field.isDateBased(), now.isSupported(field));
    }

    @Test
    void ofInstant() {
        var instant = Instant.parse("2022-03-19T21:23:24Z");
        var dateTime = LocalDateDecimalTime.ofInstant(instant, ZoneId.of("Europe/Amsterdam"));
        assertEquals("2022-03-19Td9:32:91.576", dateTime.toString());
    }
}
