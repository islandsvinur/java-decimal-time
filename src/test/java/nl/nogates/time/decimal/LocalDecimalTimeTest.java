package nl.nogates.time.decimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;

import static org.junit.jupiter.api.Assertions.*;

public class LocalDecimalTimeTest {
    @Test
    void hasToString() {
        LocalDecimalTime time = new LocalDecimalTime(1, 2, 3, 4);
        assertEquals("d1:02:03.000000004", time.toString());
    }

    @Test
    void doesNoValidation() {
        assertDoesNotThrow(() ->
                new LocalDecimalTime(100, 0, 0, 0)
        );
        assertDoesNotThrow(() ->
                new LocalDecimalTime(0, 100, 0, 0)
        );
        assertDoesNotThrow(() ->
                new LocalDecimalTime(0, 0, 100, 0)
        );
        assertDoesNotThrow(() ->
                new LocalDecimalTime(0, 0, 0, -100)
        );
    }

    @Test
    void ofNanoOfDay() {
        assertEquals("d0:00",
                LocalDecimalTime.ofNanoOfDay(0)
                                .toString());
        assertEquals("d0:00:00.000000001",
                LocalDecimalTime.ofNanoOfDay(1)
                                .toString());
        assertEquals("d5:00",
                LocalDecimalTime.ofNanoOfDay(43_200L * 1000_000_000L)
                                .toString());
        assertEquals("d9:99:99.863999999",
                LocalDecimalTime.ofNanoOfDay(86_400L * 1000_000_000L - 1)
                                .toString());
        assertThrows(DateTimeException.class,
                () -> LocalDecimalTime.ofNanoOfDay(-1));
        assertThrows(DateTimeException.class,
                () -> LocalDecimalTime.ofNanoOfDay(86_400L * 1000_000_000L));
    }

    @Test
    void convertFromOtherTemporalAccessor() {
        assertEquals("d5:00", LocalDecimalTime.from(LocalTime.NOON)
                                              .toString());
    }

    @Test
    void creationFactories() {
        assertEquals("d3:00",
                LocalDecimalTime.of(3)
                                .toString());
        assertEquals("d3:74",
                LocalDecimalTime.of(3, 74)
                                .toString());
        assertEquals("d3:74:82",
                LocalDecimalTime.of(3, 74, 82)
                                .toString());
        assertEquals("d3:74:82.000535232",
                LocalDecimalTime.of(3, 74, 82, 535_232)
                                .toString());

        assertThrows(DateTimeException.class, () -> LocalDecimalTime.of(-1));
        assertThrows(DateTimeException.class, () -> LocalDecimalTime.of(10));
        assertThrows(DateTimeException.class, () -> LocalDecimalTime.of(3, -1));
        assertThrows(DateTimeException.class, () -> LocalDecimalTime.of(3, 100));
        assertThrows(DateTimeException.class, () -> LocalDecimalTime.of(3, 74, -1));
        assertThrows(DateTimeException.class, () -> LocalDecimalTime.of(3, 74, 100));
    }

    @Test
    void implementsTemporalAccessor() {
        var time = LocalDecimalTime.of(4, 73, 89, 321_492_923);
        assertTrue(time.isSupported(ChronoField.NANO_OF_DAY));
        assertTrue(time.isSupported(ChronoField.MICRO_OF_DAY));
        assertTrue(time.isSupported(ChronoField.MILLI_OF_DAY));
        assertEquals(40944417492923L, time.getLong(ChronoField.NANO_OF_DAY));
        assertEquals(40944417492L, time.getLong(ChronoField.MICRO_OF_DAY));
        assertEquals(40944417L, time.getLong(ChronoField.MILLI_OF_DAY));
    }

    @Test
    void with() {
        var time = LocalDecimalTime.of(4, 73, 89, 321_492_923);

        assertEquals("d4:73:89.000000003",
                time.with(DecimalChronoField.NANO_OF_DECIMAL_SECOND, 3)
                    .toString());
        assertEquals("d4:73:89.000003",
                time.with(DecimalChronoField.MICRO_OF_DECIMAL_SECOND, 3)
                    .toString());
        assertEquals("d3:73:89.321492923",
                time.with(DecimalChronoField.DECIMAL_HOUR_OF_DAY, 3)
                    .toString());
        assertEquals("d4:03:89.321492923",
                time.with(DecimalChronoField.DECIMAL_MINUTE_OF_DECIMAL_HOUR, 3)
                    .toString());
        assertEquals("d4:73:03.321492923",
                time.with(DecimalChronoField.DECIMAL_SECOND_OF_DECIMAL_MINUTE, 3)
                    .toString());
        assertEquals("d0:03:89.321492923",
                time.with(DecimalChronoField.DECIMAL_MINUTE_OF_DAY, 3)
                    .toString());
        assertEquals("d0:00:03.321492923",
                time.with(DecimalChronoField.DECIMAL_SECOND_OF_DAY, 3)
                    .toString());

        assertEquals("d0:00:00.000000003",
                time.with(ChronoField.NANO_OF_DAY, 3)
                    .toString());
        assertThrows(UnsupportedTemporalTypeException.class,
                () -> time.with(ChronoField.NANO_OF_SECOND, 3));
        assertThrows(UnsupportedTemporalTypeException.class,
                () -> time.with(ChronoField.MINUTE_OF_HOUR, 3));
    }

    @Test
    void plus() {
        var time = LocalDecimalTime.of(4, 73, 89, 0);

        assertEquals("d4:73:92",
                time.plus(3, DecimalChronoUnit.DECIMAL_SECONDS)
                    .toString());
        assertEquals("d4:76:89",
                time.plus(3, DecimalChronoUnit.DECIMAL_MINUTES)
                    .toString());
        assertEquals("d7:73:89",
                time.plus(3, DecimalChronoUnit.DECIMAL_HOURS)
                    .toString());

        assertEquals("d4:74:12",
                time.plus(23, DecimalChronoUnit.DECIMAL_SECONDS)
                    .toString());
        assertEquals("d5:04:89",
                time.plus(31, DecimalChronoUnit.DECIMAL_MINUTES)
                    .toString());
        assertEquals("d2:73:89",
                time.plus(8, DecimalChronoUnit.DECIMAL_HOURS)
                    .toString());

        assertEquals("d4:73:89.000000849",
                time.plus(849, ChronoUnit.NANOS)
                    .toString());
        assertEquals("d4:73:89.000849",
                time.plus(849, ChronoUnit.MICROS)
                    .toString());
        assertEquals("d4:73:89.849",
                time.plus(849, ChronoUnit.MILLIS)
                    .toString());

        assertThrows(DateTimeException.class,
                () -> time.plus(1849, ChronoUnit.MILLIS));
    }

    @Test
    void until() {
        var time = LocalDecimalTime.of(4, 73, 89, 0);
        var other = LocalDecimalTime.of(9, 43, 12, 782432032);

        assertEquals(46923, time.until(other, DecimalChronoUnit.DECIMAL_SECONDS));
        assertEquals(469, time.until(other, DecimalChronoUnit.DECIMAL_MINUTES));
        assertEquals(4, time.until(other, DecimalChronoUnit.DECIMAL_HOURS));

        assertEquals(40542254432032L, time.until(other, ChronoUnit.NANOS));
        assertEquals(40542254432L, time.until(other, ChronoUnit.MICROS));
        assertEquals(40542254L, time.until(other, ChronoUnit.MILLIS));

        assertThrows(UnsupportedTemporalTypeException.class, () -> time.until(other, ChronoUnit.SECONDS));
    }

    @Test
    void adjustInto() {
        var time = LocalDecimalTime.MIN;

        assertEquals("d0:00",
                LocalTime.of(0, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d0:00:00.000000001",
                LocalTime.of(0, 0, 0, 1)
                         .adjustInto(time)
                         .toString());
        assertEquals("d1:25",
                LocalTime.of(3, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d2:50",
                LocalTime.of(6, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d3:75",
                LocalTime.of(9, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d5:00",
                LocalTime.of(12, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d6:25",
                LocalTime.of(15, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d7:50",
                LocalTime.of(18, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d8:75",
                LocalTime.of(21, 0)
                         .adjustInto(time)
                         .toString());
        assertEquals("d9:99:99.863999999",
                LocalTime.of(23, 59, 59, 999999999)
                         .adjustInto(time)
                         .toString());
    }

    @Test
    void conversion() {
        assertEquals(
                LocalDecimalTime.of(8, 15, 54, 344_000_000),
                LocalDecimalTime.from(LocalTime.of(19, 34, 23))
        );
        assertEquals(
                LocalTime.of(23, 40, 56, 64_000_000),
                LocalTime.from(LocalDecimalTime.of(9, 86, 76))
        );
    }

    @ParameterizedTest(name = "supported " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    @EnumSource
    void isSupportedDecimalChronoField(DecimalChronoField field) {
        var now = LocalDecimalTime.ofNanoOfDay(LocalTime.now()
                                                        .toNanoOfDay());
        assertTrue(now.isSupported(field));
    }

    @ParameterizedTest(name = "not supported " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    @EnumSource(mode = EnumSource.Mode.EXCLUDE, names = {"NANO_OF_DAY", "MICRO_OF_DAY", "MILLI_OF_DAY"})
    void isNotSupportedChronoField(ChronoField field) {
        var now = LocalDecimalTime.ofNanoOfDay(LocalTime.now()
                                                        .toNanoOfDay());
        assertFalse(now.isSupported(field));
    }

    @ParameterizedTest(name = "supported " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    @EnumSource(names = {"NANO_OF_DAY", "MICRO_OF_DAY", "MILLI_OF_DAY"})
    void isSupportedChronoField(ChronoField field) {
        var now = LocalDecimalTime.ofNanoOfDay(LocalTime.now()
                                                        .toNanoOfDay());
        assertTrue(now.isSupported(field));
    }
}
